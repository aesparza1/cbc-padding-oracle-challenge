#!/usr/bin/env python3

from flask import Flask, request
from flask import jsonify
import secrets

from Crypto.Cipher import AES  # requires PyCryptodome
from Crypto.Util.Padding import pad, unpad
import base64

app=Flask(__name__)

SECRET = secrets.token_bytes(AES.block_size)

#from Top 100 Greatest Movie Quotes of All Time
string_list = [
    b'Magic Mirror on the wall, who is the fairest one of all?', 
    b'Our work continues...',
    b'May the Force be with you.',
    b'Carpe diem. Seize the day, boys. Make your lives extraordinary.',
    b'Today, I consider myself the luckiest man on the face of the earth.',
    b'Life is a banquet, and most poor suckers are starving to death!',
    b'Keep your friends close, but your enemies closer.',
    b'My mama always said life was like a box of chocolates. You never know what you\'re gonna get.',
    b'There\'s no place like home.',
    b'I\'ll get you, my pretty, and your little dog too!',
    b'La-dee-da, la-dee-da.',
    b'One morning I shot an elephant in my pajamas. How he got in my pajamas, I don\'t know']

def generate_cipher():
    string = secrets.choice(string_list)
    cipher = AES.new(SECRET, AES.MODE_CBC)
    ct = cipher.encrypt(pad(string, AES.block_size))
    return cipher.iv, ct

@app.route('/get_cipher', methods=['GET'])
def get_cipher():
    i, c = generate_cipher()
    return jsonify(iv=base64.b64encode(i).decode("utf-8"), cipherText=base64.b64encode(c).decode("utf-8"))

def validate_cipher_padding(iv, cipherText):
    cipher = AES.new(SECRET, AES.MODE_CBC, iv)
    plainText = cipher.decrypt(cipherText)
    #The vulnerability is right here: the unpad() function returns an error is padding is invalid
    try:
        plainText_upad = unpad(plainText, AES.block_size)
    except Exception as e:
        return str(e)
    return 'success'

@app.route('/submit_cipher', methods=['POST'])
def submit_cipher():
    j = request.get_json()
    iv = base64.b64decode(j['iv'])
    cipherText = base64.b64decode(j['cipherText'])
    msg = validate_cipher_padding(iv, cipherText)
    return jsonify(message=msg)

@app.route('/')
def func():
    return 'I\'m the CBC Padding Oracle'
       
if __name__=='__main__':
    app.run(debug=True, port = 9393) 


