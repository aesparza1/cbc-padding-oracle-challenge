
To run these scripts follow these steps:

1) Install the required Python modules. Run
	$ pip install -r requirements.txt

2) Open up 2 separate command line terminals

3) On one terminal run the oracle by starting the flask server
	$ python server.py

4) If you want to see the solution, run solution.py in the other terminal
	$ python solution.py
	
	If you want to see verbose output, run
	$ python solution.py -v

5) Or, if you prefer to run the challenge script after completing the challenge run 
	$ python challenge.py



