#!/usr/bin/env python3

from base64 import b64decode
from Crypto.Util.Padding import unpad
from Crypto.Cipher import AES 
import base64
import requests
import json

SIZE = AES.block_size
PORT = "9393"

#XOR 2 blocks of bytes
def byte_xor(b1, b2):
    return bytes([a ^ b for a, b in zip(b1, b2)])



'''
Gets padding feedback from the oracle. Sends the iv and cipher text
to the endpoint at /submit_cipher. If the response is "success", the padding is good.
Otherwise, the padding is considered invalid.
'''
def is_valid_padding(iv, ciphertxt):
    data = {'iv': base64.b64encode(iv).decode("utf-8"), 'cipherText': base64.b64encode(ciphertxt).decode("utf-8")}
    response = requests.post(f"http://localhost:{PORT}/submit_cipher", json=data).json()
    msg = response['message']
    isValid = True if (msg == 'success') else False
    return isValid




def enumerate_pad_values(iv_altered, p, block):
    '''
    Enumerate all possible hex values from 0x00...0xff at the given index p
    If we find that the altered IV results in valid padding, we return the
    XOR of c and p. 
    This is in line with the equation Dec(C_i) = C_i-1 ^ P_i

    The variable "p" represents the index where the padding left off.
    So if p = 3, the altered IV in array format looks like

    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, c, (3 ^ A), (3 ^ B)]

    where A = Dec(C_i) at the 14-th byte
	  B = Dec(C_i) at the 15-th byte

    the value "c" is the character guess we make. 
    For each guess "c", check the padding by calling, is_valid_padding()
    If the oracle indicates that the padding is valid, return c ^ p
    If the padding is invalid, continue guessing
    '''
    return



def attack_block(block):
    '''
    For each byte inside the cipher block, call the enumerate_pad_values() 
    function which submits altered IVs to the oracle. 
    
    Once a valid pad value is found, the function will return that value
    which we can use to build our decrypted block
    '''
    return



def run_padding_oracle_attack(ciphertxt):
    '''
    Split up cipher text into equal sized blocks of length SIZE
    For each block we run the "attack_block()" function which gives us
    back a decrypted block. This decrypted block will help us decrypt the
    entire cipher text. 
    '''    
    return
  
    
def run():

    #Get request to server fetches a random ciphertext
    response = requests.get(f"http://localhost:{PORT}/get_cipher").json()
    iv = base64.b64decode(response["iv"])
    ciphertxt = base64.b64decode(response["cipherText"])
    
    print('Starting padding oracle attacking')    
    print('This might take some time...')
    
    #run the padding oracle attack
    #"cracked_xor_key" is simply all the decrypted bytes. We use this to XOR
    #with the cipher text and get back the plain text
    cracked_xor_key = run_padding_oracle_attack(ciphertxt)
    
    if ( cracked_xor_key is None ):
        print('Error! oracle attack function returned empty string. This is the default configuration.')
        print('Please complete the challenge to decrypt the cipher text.')
    else:
        #once we have our cracked key, xor it with the original iv & ciphertext to get back the plaintext
        #plaintext = (iv + ciphertext) ^ cracked_xor_key
        plaintext = unpad( byte_xor(iv + ciphertxt, cracked_xor_key), SIZE).decode("utf-8")
        print('####################################')
        print('Attack finished!')
        print('Summary:')
        print('####################################')
        print("Ciphertext: ", ciphertxt)
        print("Ciphertext length: ", len(ciphertxt))
        print("IV (original): ", iv)
        print("Cracked XOR Key: ", cracked_xor_key)
        print("Block Size: ", SIZE)
        print("Plaintext:", plaintext)

run()

'''
Additional explainations on the math (read this if you want to know more)

Let i be i-th block
Let C_i be the i-th cipher text block
Let P_i be the i-th plain text block
Let Dec(C_i) be the result of decrypting C_i using the oracle's secret key
	Note: only the server does this part because only it has the secret key
	
Let P_i = Dec(C_i) ^ C_i-1 
	where C_0 = IV; The initialization vector is used as the "first" cipher text block 


How can we exploit this?

Let {C_i-1}* be the C_i-1 modified by the attacker
Let {P_i}* be the altered plaintext resulting from Dec(C_i) ^ (C_i-1)*

As a result we now have 2 equations

P_i    = Dec(C_i) ^  C_i-1
{P_i}* = Dec(C_i) ^ {C_i-1}*

Goal is to solve for Dec(C_i) 
We do this be submitting different values of {C_i-1}* and recording the response of the oracle
If the oracle's response indicates that {P_i}* has valid padding, 
then the second equation will now have a valid solution. We can now solve for Dec(C_i)

Dec(C_i) = {P_i}* ^ {C_i-1}*

We can now substitue Dec(C_i) into the first equation to solve for P_i

P_i = ( {P_i}* ^ {C_i-1}* ) ^  C_i-1

Repeat the same process for each i-th block to get the entire plaintext
'''


