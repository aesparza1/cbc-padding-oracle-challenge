#!/usr/bin/env python3

from base64 import b64decode
from Crypto.Util.Padding import unpad
from Crypto.Cipher import AES 
import base64
import requests
import json
import sys

SIZE = AES.block_size
PORT = "9393"
IS_VERBOSE = False
CURR_BLOCK = 0

if (len(sys.argv) > 1):
   IS_VERBOSE = True if (sys.argv[1] == '--verbose' or sys.argv[1] == 'verbose' or sys.argv[1] == '-v') else False

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'

#XOR 2 blocks of bytes
def byte_xor(b1, b2):
    return bytes([a ^ b for a, b in zip(b1, b2)])



'''
Gets padding feedback from the oracle. Sends the iv and cipher text
to the endpoint at /submit_cipher. If the response is "success", the padding is good.
Otherwise, the padding is considered invalid.
'''
def is_valid_padding(iv, ciphertxt, i):
    data = {'iv': base64.b64encode(iv).decode("utf-8"), 'cipherText': base64.b64encode(ciphertxt).decode("utf-8")}
    response = requests.post(f"http://localhost:{PORT}/submit_cipher", json=data).json()
    msg = response['message']
    isValid = True if (msg == 'success') else False
    if (IS_VERBOSE):
        print('-------------------------------------')
        print('Check padding')
        print('Block: ', CURR_BLOCK)
        print('Index: ', i)
        print('Pad value: ', i)
        color_msg = f"{bcolors.OKGREEN}{msg}{bcolors.ENDC}" if (isValid) else msg
        print('Oracle response: ' + color_msg)
        i1 = str(iv[:-i])[2:-1]
        i2 = str(bytes([iv[-i]]))[2:-1]
        i3 = str(iv[-i+1:])[2:-1]
        color_msg = f"{i1}{bcolors.BOLD}{bcolors.OKCYAN}{i2}{bcolors.ENDC}{bcolors.ENDC}{i3}" 
        print(f"iv altered:", color_msg)
        print('ciphertext block: ', ciphertxt)
    return isValid




'''
Enumerate all possible hex values from 0x00...0xff at the given index p
If we find that the altered IV results in valid padding, we return the
XOR of c and p. 
This is in line with the equation Dec(C_i) = C_i-1 ^ P_i

The variable "p" represents the index where the padding left off.
So if p = 3, the altered IV in array format looks like

[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, c, (3 ^ A), (3 ^ B)]

where A = Dec(C_i) at the 14-th byte
      B = Dec(C_i) at the 15-th byte

the value "c" is the character guess we make. Once the oracle informs us that
the padding is valid, we xor c ^ p
'''
def enumerate_pad_values(iv_altered, p, block):
    for c in range(256):
        iv_altered[-p] = c
        if (is_valid_padding(bytes(iv_altered), block, p)):
            break
    print(list(iv_altered))
    return c ^ p



'''
This decrypts a single fixed size block
'''
def attack_block(block):
    print('--------------------------------')
    print('attacking cipher text block: ', block)
    key_block = bytearray(SIZE)
    iv_altered = bytearray(SIZE)
    for p in range(1, len(key_block) + 1):
        for j in range(p):
            iv_altered[-j] = key_block[-j] ^ p 
        key_block[-p] = enumerate_pad_values(iv_altered, p, block)
    return key_block



'''
Split up cipher text into equal sized blocks
For each block we run the "attack_block(b)" function which gives us
a piece of the "cracked_xor_key". This key will help us decrypt the
entire cipher text. 
Notice that we don't need the original iv to carry out the attack.
But we do need the iv after the attack is finished in order to 
decrypt the ciphertext.
'''    
def run_padding_oracle_attack(ciphertxt):
    result = b''
    cracked_xor_key = b''
    blocks = [ ciphertxt[i: i + SIZE] for i in range(0, len(ciphertxt), SIZE) ]
    for b in blocks:
        key_block = attack_block(b)
        cracked_xor_key += key_block
        global CURR_BLOCK
        CURR_BLOCK += 1
    return cracked_xor_key
  
    
def run():

    #Get request to server fetches a random ciphertext
    response = requests.get(f"http://localhost:{PORT}/get_cipher").json()
    iv = base64.b64decode(response["iv"])
    ciphertxt = base64.b64decode(response["cipherText"])
    
    print('Starting padding oracle attacking')    
    print('This might take some time...')
    print('Consider running it with the "-v" option to get more verbose output')
    
    #run the padding oracle attack
    cracked_xor_key = run_padding_oracle_attack(ciphertxt)
    
    #once we have our cracked key, xor it with the original iv & ciphertext to get back the plaintext
    #plaintext = (iv + ciphertext) ^ cracked_xor_key
    plaintext = unpad( byte_xor(iv + ciphertxt, cracked_xor_key), SIZE).decode("utf-8")
    
    print('####################################')
    print('Attack finished!')
    print('Summary:')
    print('####################################')
    print(f"{bcolors.BOLD}Ciphertext:{bcolors.ENDC} ", ciphertxt)
    print(f"{bcolors.BOLD}Ciphertext length:{bcolors.ENDC} ", len(ciphertxt))
    print(f"{bcolors.BOLD}IV (original):{bcolors.ENDC} ", iv)
    print(f"{bcolors.BOLD}Cracked XOR Key:{bcolors.ENDC} ", cracked_xor_key)
    print(f"{bcolors.BOLD}Block Size:{bcolors.ENDC} ", SIZE)
    print(f"{bcolors.BOLD}verbose output?{bcolors.ENDC} ", IS_VERBOSE)
    print(f"{bcolors.BOLD}Plaintext:{bcolors.ENDC}", f"{bcolors.OKGREEN}{plaintext}{bcolors.ENDC}")

run()

'''
Additional explainations on the math (read this if you want to know more)

Let i be i-th block
Let C_i be the i-th cipher text block
Let P_i be the i-th plain text block
Let Dec(C_i) be the result of decrypting C_i using the oracle's secret key
	Note: only the server does this part because only it has the secret key
	
Let P_i = Dec(C_i) ^ C_i-1 
	where C_0 = IV; The initialization vector is used as the "first" cipher text block 


How can we exploit this?

Let {C_i-1}* be the C_i-1 modified by the attacker
Let {P_i}* be the altered plaintext resulting from Dec(C_i) ^ (C_i-1)*

As a result we now have 2 equations

P_i    = Dec(C_i) ^  C_i-1
{P_i}* = Dec(C_i) ^ {C_i-1}*

Goal is to solve for Dec(C_i) 
We do this be submitting different values of {C_i-1}* and recording the response of the oracle
If the oracle's response indicates that {P_i}* has valid padding, 
then the second equation will now have a valid solution. We can now solve for Dec(C_i)

Dec(C_i) = {P_i}* ^ {C_i-1}*

We can now substitue Dec(C_i) into the first equation to solve for P_i

P_i = ( {P_i}* ^ {C_i-1}* ) ^  C_i-1

Repeat the same process for each i-th block to get the entire plaintext
'''


